package com.blank.moviemandiri.home

import androidx.lifecycle.Observer
import androidx.lifecycle.asLiveData
import androidx.paging.DifferCallback
import androidx.paging.NullPaddedList
import androidx.paging.PagingData
import androidx.paging.PagingDataDiffer
import com.blank.moviemandiri.domain.model.DomainResource
import com.blank.moviemandiri.domain.model.GenreModel
import com.blank.moviemandiri.domain.model.ResultMovieModel
import com.blank.moviemandiri.domain.usecase.GetGenresUseCase
import com.blank.moviemandiri.domain.usecase.GetMoviesByGenreUseCase
import com.blank.moviemandiri.home.ui.HomeViewModel
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals


@ExperimentalCoroutinesApi
class HomeViewModelUnitTest : BaseTest() {
    private lateinit var viewModelTest: HomeViewModel
    private val mockGetListMovieUseCase: GetMoviesByGenreUseCase = mockk()
    private val mockGetGenresUseCase: GetGenresUseCase = mockk()

    private val observerGenres = mockk<Observer<DomainResource<List<GenreModel>>>>(relaxed = true)
    private var mockGenres = DomainResource.Success(listOf(GenreModel("", "")))

    @Before
    fun setup() {
        coEvery {
            mockGetGenresUseCase()
        } returns flowOf(mockGenres)

        viewModelTest = HomeViewModel(mockGetGenresUseCase, mockGetListMovieUseCase)
        viewModelTest.genres.observeForever(observerGenres)
    }


    @Test
    fun `when  init view model and get genres should return success`() {
        verify {
            observerGenres.onChanged(mockGenres)
        }
    }

    @Test
    fun `when  init view model and get genres should return failed`() {
        val mockGenresError = DomainResource.Error(null, "ERROR SYSTEM")
        coEvery {
            mockGetGenresUseCase()
        } returns flowOf(mockGenresError)
        verify {
            observerGenres.onChanged(mockGenres)
        }
    }

    @Test
    fun `when  set genre and genre already in the list should return success`() {

        viewModelTest.setGenre("1")
        viewModelTest.setGenre("1")
        coEvery {
            mockGetGenresUseCase()
        } returns flowOf(mockGenres)
        verify {
            observerGenres.onChanged(mockGenres)
        }
    }

    @Test
    fun `Given Mock data from use case when fetch list movie then result should be to flow`() {
        val slotPagingData = slot<PagingData<ResultMovieModel>>()
        val mockMoviesObserver = mockk<Observer<PagingData<ResultMovieModel>>>(relaxed = true)
        val mockResult = listOf(ResultMovieModel())
        coEvery {
            mockGetListMovieUseCase.invoke("1")
        } returns flowOf(PagingData.from(mockResult))
        viewModelTest.movieList.asLiveData().observeForever(mockMoviesObserver)
        viewModelTest.setGenre("1")

        verify {
            mockMoviesObserver.onChanged(capture(slotPagingData))
        }
        val resultUI = runBlocking { slotPagingData.captured.collectDataForTest() }

        assertEquals(mockResult, resultUI)
    }

    private suspend fun <T : Any> PagingData<T>.collectDataForTest(): List<T> {
        val dcb = object : DifferCallback {
            override fun onChanged(position: Int, count: Int) = Unit
            override fun onInserted(position: Int, count: Int) = Unit
            override fun onRemoved(position: Int, count: Int) = Unit
        }
        val items = mutableListOf<T>()
        val dif = object : PagingDataDiffer<T>(dcb, Dispatchers.Default) {
            override suspend fun presentNewList(
                previousList: NullPaddedList<T>,
                newList: NullPaddedList<T>,
                lastAccessedIndex: Int,
                onListPresentable: () -> Unit
            ): Int? {
                for (idx in 0 until newList.size)
                    items.add(newList.getFromStorage(idx))
                onListPresentable()
                return null
            }
        }
        dif.collectFrom(this)
        return items
    }
}
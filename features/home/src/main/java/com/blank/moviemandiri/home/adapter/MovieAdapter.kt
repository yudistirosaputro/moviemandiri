package com.blank.moviemandiri.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.blank.moviemandiri.domain.model.ResultMovieModel
import com.blank.moviemandiri.home.databinding.ItemMovieBinding
import com.blank.moviemandiri.uikit.util.formatDateMMMdd
import com.blank.moviemandiri.uikit.util.loadWithGlide

class MovieAdapter(private val onItemClick: OnItemClick) : PagingDataAdapter<
        ResultMovieModel,
        MovieAdapter.MovieViewHolder>(COMPARATOR) {

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<ResultMovieModel>() {
            override fun areItemsTheSame(
                oldItem: ResultMovieModel,
                newItem: ResultMovieModel
            ): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: ResultMovieModel,
                newItem: ResultMovieModel
            ): Boolean =
                oldItem == newItem
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(
            ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        getItem(position)?.let { holder.bindTo(it, onItemClick) }
    }


    class MovieViewHolder(private val binding: ItemMovieBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindTo(data: ResultMovieModel, onItemClick: OnItemClick) = with(binding) {
            imageItem.loadWithGlide(data.posterPath)
            textMovieName.text = data.title
            textDateRelease.formatDateMMMdd(data.releaseDate)

            root.setOnClickListener {
                onItemClick.invoke(data.id)
            }
        }
    }

}


fun interface OnItemClick {
    fun invoke(id: Int)
}

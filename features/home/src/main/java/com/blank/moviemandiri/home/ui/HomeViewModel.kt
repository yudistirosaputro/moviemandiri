package com.blank.moviemandiri.home.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.blank.moviemandiri.domain.model.DomainResource
import com.blank.moviemandiri.domain.model.GenreModel
import com.blank.moviemandiri.domain.model.ResultMovieModel
import com.blank.moviemandiri.domain.usecase.GetGenresUseCase
import com.blank.moviemandiri.domain.usecase.GetMoviesByGenreUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getGenresUseCase: GetGenresUseCase,
    private val getMoviesByGenreUseCase: GetMoviesByGenreUseCase) :
    ViewModel() {
    private val _genres: MutableLiveData<DomainResource<List<GenreModel>>> = MutableLiveData()
    val genres: LiveData<DomainResource<List<GenreModel>>> get() = _genres

    private val genresId = mutableSetOf<String>()
    private val _genreString : MutableLiveData<String> = MutableLiveData()

    val movieList: Flow<PagingData<ResultMovieModel>> =
        _genreString.asFlow()
            .distinctUntilChanged()
            .flatMapLatest {
                getMoviesByGenreUseCase(it)
            }.cachedIn(viewModelScope)

    init {
        getGenres()
    }

    fun getGenres() {
        viewModelScope.launch {
            getGenresUseCase().collect {
                _genres.value = it
            }
        }
    }

    fun setGenre(genreId: String) {
        genresId.run {
            if (contains(genreId)) remove(genreId)
            else add(genreId)
        }

        _genreString.value = genresId.joinToString(",")
    }
}
package com.blank.moviemandiri.home.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.fragment.findNavController
import com.blank.moviemandiri.domain.model.DomainResource
import com.blank.moviemandiri.domain.model.GenreModel
import com.blank.moviemandiri.home.adapter.MovieAdapter
import com.blank.moviemandiri.home.databinding.FragmentHomeBinding
import com.blank.moviemandiri.uikit.adapter.PagingLoadStateAdapter
import com.blank.moviemandiri.uikit.adapter.observeLoadStateError
import com.google.android.material.chip.Chip
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import  com.blank.movie.uikit.R as RuiKit

@AndroidEntryPoint
class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val homeViewModel: HomeViewModel by viewModels()

    private lateinit var adapterMovie: MovieAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        fetchData()
        observerData()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    private fun observerData() {
        homeViewModel.apply {
            genres.observe(viewLifecycleOwner) { data ->
                when (data) {
                    is DomainResource.Loading -> {}
                    is DomainResource.SuccessNoData -> {
                        binding.vfContent.displayedChild = VIEWFLIPPER_INDEX_ERROR
                        binding.layoutError.apply {
                            textErrorMessage.text =
                                resources.getString(RuiKit.string.empty_data)
                            ivRefreshIcon.visibility = View.GONE
                        }
                    }

                    is DomainResource.Success -> {
                        binding.vfContent.displayedChild = VIEWFLIPPER_INDEX_NORMAL
                        setViewChip(data.data)
                    }

                    is DomainResource.Error -> {
                        binding.vfContent.displayedChild = VIEWFLIPPER_INDEX_ERROR
                        binding.layoutError.apply {
                            textErrorMessage.text = data.message
                            ivRefreshIcon.setOnClickListener {
                                getGenres()
                                binding.vfContent.displayedChild = VIEWFLIPPER_INDEX_NORMAL
                            }
                        }
                    }
                }

            }
        }
    }

    private fun setViewChip(genre: List<GenreModel>) = with(homeViewModel) {
        setGenre("")
        for (i in genre.indices) {
            val chip = layoutInflater.inflate(
                com.blank.movie.uikit.R.layout.item_chip,
                binding.chipGroupCategory,
                false
            ) as Chip
            chip.text = genre[i].name

            chip.setOnCheckedChangeListener { _, _ ->
                setGenre(genre[i].id)
            }
            binding.chipGroupCategory.addView(chip)
        }
    }

    private fun fetchData() {
        viewLifecycleOwner.lifecycleScope.launch {
            homeViewModel.movieList.collectLatest {
                adapterMovie.submitData(it)
            }
        }
    }

    private fun initView() = with(binding) {
        adapterMovie = MovieAdapter {
            val request = NavDeepLinkRequest.Builder
                .fromUri("android-app://com.blank.moviemandiri.detail.ui.detailmovie/DetailMovieFragment/$it".toUri())
                .build()
            findNavController().navigate(request)
        }
        rvMovie.adapter = adapterMovie.apply {
            withLoadStateFooter(
                PagingLoadStateAdapter(retryCallback = { retry() })
            )
            observeLoadStateError(viewLifecycleOwner) {
                Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
            }
        }

    }

    companion object {
        private const val VIEWFLIPPER_INDEX_NORMAL = 0
        private const val VIEWFLIPPER_INDEX_ERROR = 1
    }
}
plugins {
    id("moviemandiri.android.feature")
    id("moviemandiri.android.library.jacoco")
}
android {
    namespace = "com.blank.moviemandiri.detail"
    buildFeatures.dataBinding = true
}

dependencies {

    implementation(project(":uikit"))
    implementation(libs.core.ktx)
    implementation(libs.appcompat)
    implementation(libs.material)
    implementation(libs.constraintlayout)
    implementation(libs.androidx.navigation.fragment.ktx)
    implementation(libs.androidx.navigation.ui.ktx)
    implementation(libs.youtube.player)
}
package com.blank.moviemandiri.detail

import androidx.lifecycle.Observer
import com.blank.moviemandiri.detail.ui.detailmovie.DetailMovieViewModel
import com.blank.moviemandiri.domain.model.DetailMovieModel
import com.blank.moviemandiri.domain.model.DomainResource
import com.blank.moviemandiri.domain.usecase.GetDetailMovieUseCase
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
class DetailMovieViewModelTest : BaseTest() {
    private lateinit var viewModel: DetailMovieViewModel
    private var mockGetDetailMovieUseCase: GetDetailMovieUseCase = mockk()


    @Before
    fun setup() {
        viewModel = DetailMovieViewModel(mockGetDetailMovieUseCase)
    }

    @Test
    fun `Get Detail movie from use case with give id movie should return success `() {
        val result = DomainResource.Success(DetailMovieModel())
        val mockkDetailMovieObserver =
            mockk<Observer<DomainResource<DetailMovieModel>>>(relaxed = true)
        coEvery {
            mockGetDetailMovieUseCase.invoke(1)
        } returns flowOf(result)
        viewModel.detailMovieData.observeForever(mockkDetailMovieObserver)
        viewModel.getDetailMovie(1)
        verify {
            mockkDetailMovieObserver.onChanged(result)
        }
    }

    @Test
    fun `Get Detail movie from use case with give id movie should return error `() {
        val result = DomainResource.Error(error = null, message = "ERROR Server")
        val mockkDetailMovieObserver =
            mockk<Observer<DomainResource<DetailMovieModel>>>(relaxed = true)
        coEvery {
            mockGetDetailMovieUseCase.invoke(1)
        } returns flowOf(result)
        viewModel.detailMovieData.observeForever(mockkDetailMovieObserver)
        viewModel.getDetailMovie(1)
        verify {
            mockkDetailMovieObserver.onChanged(result)
        }
    }
}
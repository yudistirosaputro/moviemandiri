package com.blank.moviemandiri.detail.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.blank.moviemandiri.detail.databinding.ItemReviewBinding
import com.blank.moviemandiri.domain.model.ReviewModel
import com.blank.moviemandiri.uikit.util.loadWithGlide

class ReviewMovieAdapter : PagingDataAdapter<
        ReviewModel,
        ReviewMovieAdapter.ReviewMovieViewHolder>(COMPARATOR) {

    companion object {
        private val COMPARATOR = object : DiffUtil.ItemCallback<ReviewModel>() {
            override fun areItemsTheSame(
                oldItem: ReviewModel,
                newItem: ReviewModel
            ): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: ReviewModel,
                newItem: ReviewModel
            ): Boolean =
                oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewMovieViewHolder {
        return ReviewMovieViewHolder(
            ItemReviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ReviewMovieViewHolder, position: Int) {
        getItem(position)?.let { holder.bindTo(it) }
    }


    class ReviewMovieViewHolder(private val binding: ItemReviewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bindTo(data: ReviewModel) = with(binding) {

            ivProfile.loadWithGlide(data.urlImage)


            textContent.text = data.review
            textScoreItem.text = data.rating.toString()
            val title = "A Review By ${data.authorName}"
            textTitle.text = title
        }
    }
}

package com.blank.moviemandiri.detail.ui.detailmovie

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.blank.moviemandiri.detail.databinding.FragmentDetailMovieBinding
import com.blank.moviemandiri.detail.ui.adapter.RecyclerViewVideoAdapter
import com.blank.moviemandiri.domain.model.DetailMovieModel
import com.blank.moviemandiri.domain.model.DomainResource
import com.blank.moviemandiri.uikit.util.loadWithGlide
import dagger.hilt.android.AndroidEntryPoint
import  com.blank.movie.uikit.R as RuiKit


@AndroidEntryPoint
class DetailMovieFragment : Fragment() {

    private val detailMovieViewModel: DetailMovieViewModel by viewModels()
    private var _binding: FragmentDetailMovieBinding? = null
    private val binding get() = _binding!!
    private val args: DetailMovieFragmentArgs by navArgs()
    private val videoAdapter = RecyclerViewVideoAdapter()
    private var idMovie = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailMovieBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        idMovie = args.idMovie
        initData()
        initObserver()
        initView()
    }

    private fun initView() {
        binding.apply {
            backButton.setOnClickListener {
                this@DetailMovieFragment.findNavController().popBackStack()
            }
            rvVideo.adapter = videoAdapter
        }

    }

    private fun initObserver() {
        detailMovieViewModel.detailMovieData.observe(viewLifecycleOwner) { resource ->
            when (resource) {
                is DomainResource.Loading -> {
                    binding.vfContent.displayedChild = VIEWFLIPPER_INDEX_LOADING
                }

                is DomainResource.Success -> {
                    binding.vfContent.displayedChild = VIEWFLIPPER_INDEX_NORMAL
                    initItemView(resource.data)
                }

                is DomainResource.SuccessNoData -> {
                    binding.vfContent.displayedChild = VIEWFLIPPER_INDEX_ERROR
                    binding.layoutError.apply {
                        textErrorMessage.text = resources.getString(RuiKit.string.empty_data)
                        ivRefreshIcon.visibility = View.GONE
                    }
                }

                is DomainResource.Error -> {
                    binding.apply {
                        vfContent.displayedChild = VIEWFLIPPER_INDEX_ERROR
                        layoutError.apply {
                            textErrorMessage.text = resource.message
                            ivRefreshIcon.setOnClickListener {
                                initData()
                                binding.vfContent.displayedChild = VIEWFLIPPER_INDEX_LOADING
                            }
                        }
                    }

                }
            }

        }
    }

    private fun initItemView(data: DetailMovieModel) = with(binding) {
        data.apply {
            textNameItem.text = title
            textGenre.text = genre
            textScoreItem.text = voteAverage.toString()
            textSinopsisItem.text = overview
            val urlBackDrop = data.backdropPath
            imageItem.loadWithGlide(data.posterPath)
            imageBackdrop.loadWithGlide(urlBackDrop)

            videoAdapter.submitList(videosId)
            buttonReview.setOnClickListener {
                findNavController().navigate(
                    DetailMovieFragmentDirections.actionDetailMovieFragmentToReviewFragment(
                        idMovie,
                        urlBackDrop
                    )
                )
            }
        }

    }

    private fun initData() {
        detailMovieViewModel.getDetailMovie(idMovie)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    companion object {
        private const val VIEWFLIPPER_INDEX_LOADING = 0
        private const val VIEWFLIPPER_INDEX_NORMAL = 1
        private const val VIEWFLIPPER_INDEX_ERROR = 2
    }
}
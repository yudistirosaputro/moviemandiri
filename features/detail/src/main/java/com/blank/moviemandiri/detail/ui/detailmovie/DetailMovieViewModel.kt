package com.blank.moviemandiri.detail.ui.detailmovie

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.blank.moviemandiri.domain.model.DetailMovieModel
import com.blank.moviemandiri.domain.model.DomainResource
import com.blank.moviemandiri.domain.usecase.GetDetailMovieUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailMovieViewModel @Inject constructor(
    private val getDetailMovieUseCase: GetDetailMovieUseCase
) : ViewModel() {

    private val _detailMovieData: MutableLiveData<DomainResource<DetailMovieModel>> =
        MutableLiveData()
    val detailMovieData: LiveData<DomainResource<DetailMovieModel>> get() = _detailMovieData

    fun getDetailMovie(idMovie: Int) {
        viewModelScope.launch {
            getDetailMovieUseCase(idMovie)
                .collect {
                    _detailMovieData.value = it
                }

        }
    }
}
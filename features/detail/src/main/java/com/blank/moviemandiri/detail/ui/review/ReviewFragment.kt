package com.blank.moviemandiri.detail.ui.review

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.blank.moviemandiri.detail.databinding.FragmentReviewBinding
import com.blank.moviemandiri.detail.ui.adapter.ReviewMovieAdapter
import com.blank.moviemandiri.uikit.adapter.PagingLoadStateAdapter
import com.blank.moviemandiri.uikit.adapter.observeLoadStateError
import com.blank.moviemandiri.uikit.util.loadWithGlide
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


@AndroidEntryPoint
class ReviewFragment : Fragment() {
    private val viewModel: ReviewViewModel by viewModels()
    private val reviewMovieAdapter = ReviewMovieAdapter()
    private var urlBackDrop = ""
    private var _binding: FragmentReviewBinding? = null
    private val args: ReviewFragmentArgs by navArgs()
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentReviewBinding.inflate(inflater, container, false).apply {
            lifecycleOwner = viewLifecycleOwner
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData()
        initView()
        initObserver()
    }

    private fun initObserver() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.reviewsData.collectLatest {
                reviewMovieAdapter.submitData(it)
            }
        }
    }

    private fun initData() {
        val idMovie = args.idMovie
        urlBackDrop = args.urlBackdrop
        viewModel.setIdMovie(idMovie)
    }

    private fun initView() {
        binding.apply {
            backButton.setOnClickListener {
                this@ReviewFragment.findNavController().popBackStack()
            }
            rvVideo.adapter = reviewMovieAdapter.apply {
                withLoadStateFooter(
                    PagingLoadStateAdapter(retryCallback = { retry() })
                )
                observeLoadStateError(viewLifecycleOwner) {
                    Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
                }
            }
            imageBackdrop.loadWithGlide(urlBackDrop)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
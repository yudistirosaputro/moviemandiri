plugins {
    id("moviemandiri.android.application")
    id("moviemandiri.android.hilt")
    id("moviemandiri.android.application.jacoco")
}

android {
    namespace = "com.blank.moviemandiri"


    defaultConfig {
        applicationId = "com.blank.moviemandiri"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
}

dependencies {

    implementation(project(":features:home"))
    implementation(project(":features:detail"))
    implementation(project(":data"))
    implementation(project(":repository"))
    implementation(project(":domain"))
    implementation(project(":uikit"))

    implementation(libs.core.ktx)
    implementation(libs.appcompat)
    implementation(libs.material)
    implementation(libs.constraintlayout)
    implementation(libs.androidx.navigation.fragment)
    testImplementation(libs.junit)
    androidTestImplementation(libs.androidx.test.ext.junit)
    androidTestImplementation(libs.espresso.core)
}
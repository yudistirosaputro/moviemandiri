pluginManagement {
    includeBuild("build-logic")
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}

dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}


rootProject.name = "MovieMandiri"
include(":app")
include(":data")
include(":domain")
include(":repository")
include(":features:home")
include(":features:detail")
include(":uikit")
include(":apikey")

# Movie

This Project use to test only

## Screenshoot

<img src="preview/ss1.png"  width="200px" />
<img src="preview/ss2.png"  width="200px" />
<img src="preview/ss3.png"  width="200px" />
<img src="preview/ss4.png"  width="200px" />

## Architecture

The app architecture has three layers:
a [data layer](https://developer.android.com/jetpack/guide/data-layer),
a [domain layer](https://developer.android.com/jetpack/guide/domain-layer) and
a [UI layer](https://developer.android.com/jetpack/guide/ui-layer).
following the guideline
from [NowInAndroid](https://github.com/android/nowinandroid/blob/main/docs/ArchitectureLearningJourney.md)
but without offline mode

## Hide API KEY

Because the project has static api key from  [MOVIEDB](themoviedb.org) so to more secure it's store
to native lib and build in apikey.aar to protect from stolen in git

## Tech stack

* Minimum SDK level 26
* Setup Project with Kotlin DSL
  with [gradle version catalog](https://developer.android.com/build/migrate-to-catalogs#create-version)
  also setting up convention plugins in `build-logic`
* [Kotlin](https://kotlinlang.org/)
  based + [Coroutines](https://kotlinlang.org/docs/coroutines-guide.html) for asynchronous
* [Dagger-Hilt](https://dagger.dev/hilt/) - For Dependency Injection
* [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel/) - Manage UI
  related data in a lifecycle conscious way and act as a channel between use cases and ui
* [DataBinding](https://developer.android.com/topic/libraries/data-binding) - support library that
  allows binding of UI components in layouts to data sources,binds character details and search
  results to UI
* [Paging3](https://developer.android.com/topic/libraries/architecture/paging/v3-overview) - To load
  and display pages of data from a larger dataset from local storage or over a network
* [Kotlin Flow](https://developer.android.com/kotlin/flow) - To access data sequentially
* [Retrofit](https://square.github.io/retrofit/) - To access the Rest Api
* [Navigation Component](https://developer.android.com/guide/navigation/navigation-getting-started) -
  Android Jetpack's Navigation component helps in implementing navigation between fragments
* [MockK](https://mockk.io/) - To mocking library for Kotlin.

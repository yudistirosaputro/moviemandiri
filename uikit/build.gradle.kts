plugins {
    id("moviemandiri.android.library")
    id("moviemandiri.android.hilt")
}

android {
    namespace = "com.blank.movie.uikit"
    buildFeatures.dataBinding = true
}

dependencies {
    implementation(libs.core.ktx)
    implementation(libs.appcompat)
    implementation(libs.material)
    implementation(libs.constraintlayout)
    implementation(libs.glide.android)
    kapt(libs.glide.compiler)
}
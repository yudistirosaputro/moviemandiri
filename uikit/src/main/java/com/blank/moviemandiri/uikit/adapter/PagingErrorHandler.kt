package com.blank.moviemandiri.uikit.adapter

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

fun <T : Any, VH : RecyclerView.ViewHolder> PagingDataAdapter<T, VH>.observeLoadStateError(
    lifecycleOwner: LifecycleOwner,
    onErrorAction: (String) -> Unit
) {
    lifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
        this@observeLoadStateError.loadStateFlow.collectLatest { loadState ->
            val errorState = when {
                loadState.source.append is LoadState.Error -> loadState.source.append as LoadState.Error
                loadState.source.prepend is LoadState.Error -> loadState.source.prepend as LoadState.Error
                loadState.append is LoadState.Error -> loadState.append as LoadState.Error
                loadState.prepend is LoadState.Error -> loadState.prepend as LoadState.Error
                loadState.refresh is LoadState.Error -> loadState.refresh as LoadState.Error
                else -> null
            }

            errorState?.error?.message?.let {
                onErrorAction(it)
            }
        }
    }
}
package com.blank.moviemandiri.uikit.util

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.widget.ImageView
import android.widget.TextView
import com.blank.movie.uikit.BuildConfig
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import java.text.SimpleDateFormat
import java.util.Locale

const val DEFAULT_FORMAT_DATE = "MMM dd,yyyy"
const val DEFAULT_UNFORMATTED_DATE = "yyyy-mm-dd"
fun ImageView.loadWithGlide(url:String) {
    Glide.with(this)
        .load(BuildConfig.BASE_URL_IMAGE + url)
        .placeholder(ColorDrawable(Color.BLACK))
        .centerCrop()
        .transition(withCrossFade())
        .into(this)
}

fun TextView.formatDateMMMdd(dateUnFormatted:String,unformattedDate:String = DEFAULT_UNFORMATTED_DATE,
                             formattedDate:String = DEFAULT_FORMAT_DATE){
    val date = SimpleDateFormat(unformattedDate, Locale.getDefault()).parse(dateUnFormatted)
    this.text = SimpleDateFormat(formattedDate, Locale.getDefault()).format(date)
}
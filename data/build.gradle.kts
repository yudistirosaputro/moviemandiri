plugins {
    id("moviemandiri.android.library")
    id("moviemandiri.android.hilt")
    id("moviemandiri.android.library.jacoco")
}

android {
    namespace = "com.blank.moviemandiri.data"
}

dependencies {
    api(project(":apikey"))
    implementation(libs.kotlinx.coroutines.android)
    implementation(libs.bundles.network)
    implementation(libs.kotlinx.serialization.json)
}
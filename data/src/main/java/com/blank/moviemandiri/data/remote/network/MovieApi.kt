package com.blank.moviemandiri.data.remote.network

import com.blank.moviemandiri.data.model.BaseResponsePaging
import com.blank.moviemandiri.data.model.ErrorResponse
import com.blank.moviemandiri.data.model.GenresResponse
import com.blank.moviemandiri.data.model.NetworkResponse
import com.blank.moviemandiri.data.model.ResultMovieResponse
import com.blank.moviemandiri.data.model.ReviewResultResponse
import com.blank.moviemandiri.data.model.YoutubeVideoResponse
import com.blank.moviemandiri.data.model.detailmovie.DetailMovieResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieApi {

    @GET("genre/movie/list?&language=en-US")
    suspend fun getMovieGenre(): NetworkResponse<GenresResponse, ErrorResponse>

    @GET("discover/movie?language=en-US")
    suspend fun getListMovie(
        @Query("with_genres") genreId: String,
        @Query("page") page: Int
    ): NetworkResponse<BaseResponsePaging<ResultMovieResponse>, ErrorResponse>

    @GET("movie/{id}?language=en-US")
    suspend fun getDetailMovie(
        @Path("id") id: Int
    ): NetworkResponse<DetailMovieResponse, ErrorResponse>

    @GET("movie/{id}/videos?language=en-US")
    suspend fun getVideo(@Path("id") idMovie: Int): NetworkResponse<YoutubeVideoResponse, ErrorResponse>

    @GET("movie/{id}/reviews?language=en-US")
    suspend fun getReview(
        @Path("id") id: Int,
        @Query("page") page: Int
    ): NetworkResponse<BaseResponsePaging<ReviewResultResponse>, ErrorResponse>
}
package com.blank.moviemandiri.data.remote.network

import com.blank.moviemandiri.data.datasource.MovieDataSource
import com.blank.moviemandiri.data.model.BaseResponsePaging
import com.blank.moviemandiri.data.model.ErrorResponse
import com.blank.moviemandiri.data.model.GenresResponse
import com.blank.moviemandiri.data.model.NetworkResponse
import com.blank.moviemandiri.data.model.ResultMovieResponse
import com.blank.moviemandiri.data.model.ReviewResultResponse
import com.blank.moviemandiri.data.model.YoutubeVideoResponse
import com.blank.moviemandiri.data.model.detailmovie.DetailMovieResponse
import javax.inject.Inject

class MovieDataSourceImpl @Inject constructor(private val movieApi: MovieApi) : MovieDataSource {
    override suspend fun getGenres(): NetworkResponse<GenresResponse, ErrorResponse> =
        movieApi.getMovieGenre()

    override suspend fun getMovieList(
        genre: String,
        page: Int
    ): NetworkResponse<BaseResponsePaging<ResultMovieResponse>, ErrorResponse> =
        movieApi.getListMovie(genre,page)

    override suspend fun getDetailMovie(idMovie: Int): NetworkResponse<DetailMovieResponse, ErrorResponse> =
        movieApi.getDetailMovie(idMovie)

    override suspend fun getVideoData(idMovie: Int): NetworkResponse<YoutubeVideoResponse, ErrorResponse> =
        movieApi.getVideo(idMovie)

    override suspend fun getReview(
        idMovie: Int,
        page: Int
    ): NetworkResponse<BaseResponsePaging<ReviewResultResponse>, ErrorResponse> =
        movieApi.getReview(idMovie,page)
}
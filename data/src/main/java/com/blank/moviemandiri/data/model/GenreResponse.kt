package com.blank.moviemandiri.data.model

import com.google.gson.annotations.SerializedName

data class GenresResponse(
    @SerializedName("genres")
    val genres: List<Genre>
)

data class Genre(
    @SerializedName("id")
    val id:String,
    @SerializedName("name")
    val name:String
)

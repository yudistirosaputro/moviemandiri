package com.blank.moviemandiri.data.datasource

import com.blank.moviemandiri.data.model.BaseResponsePaging
import com.blank.moviemandiri.data.model.ErrorResponse
import com.blank.moviemandiri.data.model.GenresResponse
import com.blank.moviemandiri.data.model.NetworkResponse
import com.blank.moviemandiri.data.model.ResultMovieResponse
import com.blank.moviemandiri.data.model.ReviewResultResponse
import com.blank.moviemandiri.data.model.YoutubeVideoResponse
import com.blank.moviemandiri.data.model.detailmovie.DetailMovieResponse

interface MovieDataSource {

    suspend fun getGenres () : NetworkResponse<GenresResponse,ErrorResponse>

    suspend fun getMovieList(
        genre:String,
        page: Int
    ): NetworkResponse<BaseResponsePaging<ResultMovieResponse>, ErrorResponse>

    suspend fun getDetailMovie(idMovie: Int): NetworkResponse<DetailMovieResponse, ErrorResponse>
    suspend fun getVideoData(idMovie: Int): NetworkResponse<YoutubeVideoResponse, ErrorResponse>
    suspend fun getReview(
        idMovie: Int,
        page: Int
    ): NetworkResponse<BaseResponsePaging<ReviewResultResponse>, ErrorResponse>
}
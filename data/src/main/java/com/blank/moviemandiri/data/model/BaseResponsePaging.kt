package com.blank.moviemandiri.data.model

import com.google.gson.annotations.SerializedName


data class  BaseResponsePaging<T>(
    @SerializedName("id") val id: Int? = null,
    @SerializedName("page") val page: Int? = null,
    @SerializedName("results") val results: List<T>?,
    @SerializedName("total_pages") val totalPages: Int? = null,
    @SerializedName("total_results") val totalResults: Int? = null
)
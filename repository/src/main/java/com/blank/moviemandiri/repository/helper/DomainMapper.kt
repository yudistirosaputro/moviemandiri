package com.blank.moviemandiri.repository.helper

interface DomainMapper<DataModel, DomainModel> {

    fun mapToDomainModel(dataModel: DataModel): DomainModel

}
package com.blank.moviemandiri.repository.mapper


import com.blank.moviemandiri.data.model.ReviewResultResponse
import com.blank.moviemandiri.domain.model.ReviewModel
import com.blank.moviemandiri.repository.helper.DomainMapper

class ReviewMapper : DomainMapper<ReviewResultResponse, ReviewModel> {

    override fun mapToDomainModel(dataModel: ReviewResultResponse): ReviewModel {
        return with(dataModel) {
            ReviewModel(
                id = id.orEmpty(),
                urlImage = authorDetails?.avatarPath.orEmpty(),
                authorName = author.orEmpty(),
                review = content.orEmpty(),
                rating = authorDetails?.rating ?: 0
            )
        }
    }
}
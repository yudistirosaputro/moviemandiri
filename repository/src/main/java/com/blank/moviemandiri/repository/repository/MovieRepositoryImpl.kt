package com.blank.moviemandiri.repository.repository

import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.blank.moviemandiri.data.remote.network.MovieDataSourceImpl
import com.blank.moviemandiri.domain.model.DetailMovieModel
import com.blank.moviemandiri.domain.model.DomainResource
import com.blank.moviemandiri.domain.model.GenreModel
import com.blank.moviemandiri.domain.model.ResultMovieModel
import com.blank.moviemandiri.domain.model.ReviewModel
import com.blank.moviemandiri.domain.repository.MovieRepository
import com.blank.moviemandiri.repository.helper.createPager
import com.blank.moviemandiri.repository.helper.dataSourceHandling
import com.blank.moviemandiri.repository.helper.mapToPagingResource
import com.blank.moviemandiri.repository.mapper.DetailMovieMapper
import com.blank.moviemandiri.repository.mapper.GenreMapper
import com.blank.moviemandiri.repository.mapper.MovieMapper
import com.blank.moviemandiri.repository.mapper.ReviewMapper
import com.blank.moviemandiri.repository.mapper.YoutubeVideoMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class MovieRepositoryImpl @Inject constructor(private val movieDataSource: MovieDataSourceImpl) : MovieRepository {
    override fun getGenres(): Flow<DomainResource<List<GenreModel>>> {
       return dataSourceHandling(
           callApi = {movieDataSource.getGenres()},
           mapper = GenreMapper()
       )
    }

    override fun getMovieList(genres: String): Flow<PagingData<ResultMovieModel>> {
        return createPager(
            config = PagingConfig(
                pageSize = LIMIT_PAGING,
                initialLoadSize = LIMIT_PAGING
            )
        ) { page ->
            movieDataSource.getMovieList(genres,page).mapToPagingResource()
        }.flow.map {pagingData ->
            pagingData.map(MovieMapper()::mapToDomainModel)
        }

    }

    override fun getDetailMovie(idMovie: Int): Flow<DomainResource<DetailMovieModel>> {
        return dataSourceHandling(
            callApi = { movieDataSource.getDetailMovie(idMovie) },
            mapper = DetailMovieMapper()
        )
    }

    override fun getVideoData(idMovie: Int): Flow<DomainResource<List<String>>> {
        return dataSourceHandling(
            callApi = { movieDataSource.getVideoData(idMovie) },
            mapper = YoutubeVideoMapper()
        )
    }

    override fun getReview(idMovie: Int): Flow<PagingData<ReviewModel>> {
        return createPager(
            config = PagingConfig(
                pageSize = LIMIT_PAGING,
                initialLoadSize = LIMIT_PAGING
            )
        ) { page ->
            movieDataSource.getReview(idMovie,page).mapToPagingResource()
        }.flow.map {pagingData ->
            pagingData.map(ReviewMapper()::mapToDomainModel)
        }

    }

    companion object {
        private const val LIMIT_PAGING = 25 // not implement due the api request
    }
}
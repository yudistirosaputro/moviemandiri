package com.blank.moviemandiri.repository.mapper

import com.blank.moviemandiri.data.model.YoutubeVideoResponse
import com.blank.moviemandiri.repository.helper.DomainMapper
import javax.inject.Inject

class YoutubeVideoMapper  : DomainMapper<YoutubeVideoResponse, List<String>> {
    override fun mapToDomainModel(dataModel: YoutubeVideoResponse): List<String> {
        return dataModel.results.map {
            it.key.orEmpty()
        }
    }
}
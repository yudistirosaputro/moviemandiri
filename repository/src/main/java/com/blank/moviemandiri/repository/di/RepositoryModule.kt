package com.blank.moviemandiri.repository.di

import com.blank.moviemandiri.domain.repository.MovieRepository
import com.blank.moviemandiri.repository.repository.MovieRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent


@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {

    @Binds
    fun bindMovieRepository(movieRepositoryImpl: MovieRepositoryImpl) : MovieRepository
}
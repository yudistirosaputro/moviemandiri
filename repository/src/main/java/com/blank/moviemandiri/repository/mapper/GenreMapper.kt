package com.blank.moviemandiri.repository.mapper

import com.blank.moviemandiri.data.model.GenresResponse
import com.blank.moviemandiri.domain.model.GenreModel
import com.blank.moviemandiri.repository.helper.DomainMapper

class GenreMapper : DomainMapper<GenresResponse, List<GenreModel>> {
    override fun mapToDomainModel(dataModel: GenresResponse):List<GenreModel> {
        return with(dataModel) {
           genres.map { genre ->
               GenreModel(
                   id = genre.id,
                   name = genre.name
               )
           }
        }
    }

}
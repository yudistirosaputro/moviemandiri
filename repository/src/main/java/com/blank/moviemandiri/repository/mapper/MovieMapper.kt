package com.blank.moviemandiri.repository.mapper

import com.blank.moviemandiri.data.model.ResultMovieResponse
import com.blank.moviemandiri.domain.model.ResultMovieModel
import com.blank.moviemandiri.repository.helper.DomainMapper
import javax.inject.Inject

class MovieMapper @Inject constructor() : DomainMapper<ResultMovieResponse, ResultMovieModel> {
    override fun mapToDomainModel(dataModel: ResultMovieResponse): ResultMovieModel {
        return with(dataModel) {
            ResultMovieModel(
                id = id ?: 0,
                originalName = originalName.orEmpty(),
                overview = overview.orEmpty(),
                popularity = popularity ?: 0.0,
                posterPath = posterPath.orEmpty(),
                backdropPath = backdropPath.orEmpty(),
                releaseDate = releaseDate.orEmpty(),
                title = title.orEmpty(),
            )
        }
    }
}
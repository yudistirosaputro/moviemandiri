package com.blank.moviemandiri.repository.mapper

import com.blank.moviemandiri.data.model.detailmovie.DetailMovieResponse
import com.blank.moviemandiri.domain.model.DetailMovieModel
import com.blank.moviemandiri.repository.helper.DomainMapper

class DetailMovieMapper  :
    DomainMapper<DetailMovieResponse, DetailMovieModel> {
    override fun mapToDomainModel(dataModel: DetailMovieResponse): DetailMovieModel {
        return with(dataModel) {
            DetailMovieModel(
                id = id ?: 0,
                originalName = originalTitle.orEmpty(),
                overview = overview.orEmpty(),
                popularity = popularity ?: 0.0,
                posterPath = posterPath.orEmpty(),
                backdropPath = backdropPath.orEmpty(),
                releaseDate = releaseDate.orEmpty(),
                title = title.orEmpty(),
                synopsis = overview.orEmpty(),
                voteAverage = voteAverage ?: 0.0,
                genre = genres.map {
                    it.name
                }.joinToString(",")
            )
        }
    }
}
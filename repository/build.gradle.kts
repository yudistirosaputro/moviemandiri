plugins {
    id("moviemandiri.android.library")
    id("moviemandiri.android.hilt")
    id("moviemandiri.android.library.jacoco")
}

android {
    namespace = "com.blank.movie.repository"
}

dependencies {
    implementation(libs.kotlinx.coroutines.android)
    implementation(project(":data"))
    implementation(project(":domain"))
}
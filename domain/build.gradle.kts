plugins {
    id("moviemandiri.android.library")
    id("moviemandiri.android.hilt")
    id("moviemandiri.android.library.jacoco")
}

android {
    namespace = "com.blank.moviemandiri.domain"

}

dependencies {

    implementation(libs.kotlinx.coroutines.android)
    testImplementation(libs.kotlinx.coroutines.test)
}
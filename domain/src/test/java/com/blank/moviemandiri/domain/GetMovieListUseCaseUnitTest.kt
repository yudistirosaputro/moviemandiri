package com.blank.moviemandiri.domain

import androidx.paging.PagingData
import com.blank.moviemandiri.domain.model.ResultMovieModel
import com.blank.moviemandiri.domain.repository.MovieRepository
import com.blank.moviemandiri.domain.usecase.GetMoviesByGenreUseCase

import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertEquals

@ExperimentalCoroutinesApi
class GetMovieListUseCaseUnitTest : BaseTest() {
    private lateinit var getListMovieUseCase: GetMoviesByGenreUseCase
    private val movieRepository: MovieRepository = mockk()

    override fun setUp() {
        super.setUp()
        getListMovieUseCase = GetMoviesByGenreUseCase(movieRepository)
    }

    @Test
    fun `Given data list movie when call use case `() {
        val mockResult = listOf(ResultMovieModel())
        coEvery {
            movieRepository.getMovieList("1")
        } returns flowOf(PagingData.from(mockResult))

        val result = runBlocking {
            getListMovieUseCase.invoke("1").first().toList()
        }

        assertEquals(mockResult, result)
    }
}
package com.blank.moviemandiri.domain.repository

import androidx.paging.PagingData
import com.blank.moviemandiri.domain.model.DetailMovieModel
import com.blank.moviemandiri.domain.model.DomainResource
import com.blank.moviemandiri.domain.model.GenreModel
import com.blank.moviemandiri.domain.model.ResultMovieModel
import com.blank.moviemandiri.domain.model.ReviewModel
import kotlinx.coroutines.flow.Flow

interface MovieRepository {

    fun  getGenres(): Flow<DomainResource<List<GenreModel>>>
    fun getMovieList(
        genres:String
    ): Flow<PagingData<ResultMovieModel>>

    fun getDetailMovie(idMovie: Int): Flow<DomainResource<DetailMovieModel>>
    fun getVideoData(idMovie: Int): Flow<DomainResource<List<String>>>
    fun getReview(
        idMovie: Int
    ): Flow<PagingData<ReviewModel>>
}
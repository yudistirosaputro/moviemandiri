package com.blank.moviemandiri.domain.model

data class GenreModel(
    val id:String,
    val name:String
)
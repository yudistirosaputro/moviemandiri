package com.blank.moviemandiri.domain.usecase

import com.blank.moviemandiri.domain.repository.MovieRepository
import javax.inject.Inject

class GetGenresUseCase @Inject constructor(private val movieRepository: MovieRepository) {

    operator fun invoke() = movieRepository.getGenres()
}
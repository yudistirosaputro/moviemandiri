package com.blank.moviemandiri.domain.usecase

import com.blank.moviemandiri.domain.repository.MovieRepository
import javax.inject.Inject

class GetMoviesByGenreUseCase @Inject constructor(
    private val movieRepository: MovieRepository
) {
    operator fun invoke(genre:String) = movieRepository.getMovieList(genre)
}